﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO.Ports;
using System.Threading;
using UnityEngine;
using UniRx;

public class RxSerialHandler : MonoBehaviour
{
    [SerializeField]
    private string portName;

    [SerializeField]
    private int baudrate = 115200;

    private SerialPort _serialport = null;
	private Thread _thread = null;

	//private string _sensorMsg;
	private bool _isRunning = false;

    //private Subject<string> _sensorMsg = new Subject<string>();
    //   public IObservable<string> OnMessage
    //{
    //	get { return _sensorMsg; }
    //}
    private string _sensorMsg = "";
    public IReadOnlyReactiveProperty<string> SensorMsg { get; private set; }
    

    public void Open()
	{
        _serialport = new SerialPort(portName, baudrate, Parity.None, 8, StopBits.One);
		_serialport.Open();

		_serialport.DtrEnable = true;
		_serialport.RtsEnable = true;
		_serialport.DiscardInBuffer();
	}

    public void Close()
	{
        if (null != _thread)
		{
			ThreadStop();
		}

        if (null != _serialport && _serialport.IsOpen)
		{

			_serialport.Close();
			_serialport.Dispose();
		}
	}

    public void ThreadStart()
	{
        if (!_serialport.IsOpen)
		{
			return;
		}

        if (null != _thread)
		{
			ThreadStop();
		}

		_isRunning = true;

        _thread = new Thread(Read);
        _thread.Start();
    }

    public void ThreadStop()
	{
		_isRunning = false;

		if (null != _thread && _thread.IsAlive)
		{
			_thread.Join();
		}

		_thread = null;
	}


    private void Read()
	{
        while(_isRunning && null != _serialport && _serialport.IsOpen)
		{
            try
			{
                //var msg = _serialport.ReadLine();
                //            _sensorMsg.OnNext(msg);
                //            Observable.Create<string>(observer =>
                //            {
                //                observer.OnNext(msg);
                //                return null;
                //            })
                //            .ObserveOnMainThread()
                //            .Subscribe(OnReceiveMessage);
                _sensorMsg = _serialport.ReadLine();
            }
            catch (System.Exception err)
			{
				Debug.LogWarning(err.Message);
			}
		}
	}

    void Awake()
    {
        SensorMsg = this.ObserveEveryValueChanged(x => x._sensorMsg).ToReadOnlyReactiveProperty<string>();
    }

    // Start is called before the first frame update
    void Start()
    {
		
	}

    // Update is called once per frame
    void Update()
    {
        
    }
}
