﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;


public class SprayTrigger : MonoBehaviour
{

    private bool _isInjection = false;

    // Start is called before the first frame update
    void Start()
    {
        _isInjection = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.P))
        {
            // スプレー開始
            _isInjection = true;


        }
        else if (Input.GetKeyDown(KeyCode.R))
        {
            // スプレー停止
            _isInjection = false;
        }

        if(_isInjection)
        {
            RaycastHit hit;
           if( Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward), out hit, 1) )
           {
               Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.forward) * hit.distance, Color.yellow);
                var wp = hit.transform.GetComponent<WallPainter>();
                wp.Paint(hit.point, hit.distance);
           }
           else
           {
               Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.forward), Color.blue);
           }
        }
    }
}
