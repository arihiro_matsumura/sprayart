﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Es.InkPainter;
using UniRx;

[RequireComponent(typeof(InkCanvas), typeof(RxSerialHandler))]
public class WallPainter : MonoBehaviour
{
    private RxSerialHandler _serialHandler;

    [SerializeField]
    private Brush brush = null;

    private InkCanvas _canvas = null;

    [SerializeField]
    private int wait = 3;

    private int waitCount;

    void OnSerialMessage(string msg)
    {
        string[] del = { ", " };
        string[] arr = msg.Split(del, StringSplitOptions.None);
        if (3 <= arr.Length)
        {
            float r = Map(float.Parse(arr[0]), 0f, 255f, 0f, 1f);
            float g = Map(float.Parse(arr[1]), 0f, 255f, 0f, 1f);
            float b = Map(float.Parse(arr[2]), 0f, 255f, 0f, 1f);

            float h, s, v;

            Color.RGBToHSV(new Color(r, g, b), out h, out s, out v);
            brush.Color = Color.HSVToRGB(h, 1f, 1f);


            //Color color = new Color(r, g, b);
            //brush.Color = Color.HSVToRGB(h, 1f, 1f);
            //Debug.Log(brush.Color);
        }
    }

    float Map(float value, float fromMin, float fromMax, float toMin, float toMax )
    {
        return toMin + (toMax - toMin) * ((value - fromMin) / (fromMax - fromMin));
    }

    // Start is called before the first frame update
    void Start()
    {
        _canvas = GetComponent<InkCanvas>();

        _serialHandler = GetComponent<RxSerialHandler>();
        //_serialHandler.OnMessage.Subscribe(OnSerialMessage);
        _serialHandler.SensorMsg.Subscribe(OnSerialMessage);

        _serialHandler.Open();
        _serialHandler.ThreadStart();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Tab))
        {
            RefleshPaint();
        }
    }

    public void FixedUpdate()
    {
        ++waitCount;
    }

    void OnDestroy()
    {
        _serialHandler.ThreadStop();
        _serialHandler.Close();
    }

    public void RefleshPaint()
    {
        _canvas.ResetPaint();
    }

    public void Paint(Vector3 pos, float distance)
    {
        if (waitCount < wait)
            return;
        waitCount = 0;
        brush.Scale = 0.05f + distance / 10f;
        _canvas.Paint(brush, pos);
    }
}
